/**
 * 
 */
package Web_Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * @author Sihlesenkosi
 * This class represents the Home Page of my test site http://automationpractice.com
 */
public class HomePage {
	
	WebDriver driver;
	
	WebElement logoLink;
	WebElement searchBox;
	WebElement searchBtn;
	WebElement signInBtn;
	WebElement contactUsBtn;
	WebElement cartDropDwn;
	
	WebElement menuWomen;
	WebElement menuDresses;
	
	WebElement submenuCasualDresses;
	WebElement submenuEveningDresses;
	WebElement submenuSummerDresses;
	
	WebElement summerDressIcon;
	
	// Constructor that initializes the web elements on the Home Page
	public HomePage(WebDriver exampleDriver) {
		
		this.logoLink = exampleDriver.findElement(By.xpath("//*[@id=\"header_logo\"]/a/img"));
		this.searchBox = exampleDriver.findElement(By.xpath("//*[@id=\"search_query_top\"]"));
		this.searchBtn = exampleDriver.findElement(By.xpath("//*[@id=\"searchbox\"]/button"));
		this.signInBtn = exampleDriver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a"));
		this.contactUsBtn = exampleDriver.findElement(By.xpath("//*[@id=\"contact-link\"]/a"));
		this.cartDropDwn = exampleDriver.findElement(By.xpath("//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a"));
		this.menuWomen = exampleDriver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[1]/a"));
		this.menuDresses = exampleDriver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/a"));
		this.submenuCasualDresses = exampleDriver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[1]/a"));
		this.submenuEveningDresses = exampleDriver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[2]/a"));
		this.submenuSummerDresses = exampleDriver.findElement(By.xpath("//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[3]/a"));
		
	}
	
	 //This is a method for navigating back to the home page by clicking the site Logo
	public void navigatetoHomepage() {
		logoLink.click();
	}
	
	//Method to Hover over the Women Menu Item
	public void hoverWomenMenu(WebDriver exampleDriver) {
		Actions mouseActions = new Actions(exampleDriver);
		mouseActions.moveToElement(menuWomen);
	}
	
	//Method to Hover over the Dresses Menu Item
	public void hoverDressesMenu(WebDriver exampleDriver) {
		Actions mouseActions = new Actions(exampleDriver);
		mouseActions.moveToElement(menuDresses);
	}
	
	//Click on a Sign-in button method
	public void clickSignInButton() {
		signInBtn.click();
	}
	
	//Search for an item method
	public void searchForItem(String searchItem) {
		searchBox.sendKeys(searchItem);
		searchBtn.click();

	}
	
	
	
	
		
	
	

}
