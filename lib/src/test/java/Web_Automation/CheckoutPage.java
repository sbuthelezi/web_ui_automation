/**
 * 
 */
package Web_Automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author Sihlesenkosi
 * This class represents the 1st page of the Checkout process.
 */
public class CheckoutPage extends HomePage{
	
	WebElement createAccountBtn;
	WebElement signInBtn;
	WebElement emailTxtBx;
	
	
	// Constructor that initializes the web elements for this page
	public CheckoutPage(WebDriver exampleDriver) {
		super(exampleDriver);
		// TODO Auto-generated constructor stub
		createAccountBtn = exampleDriver.findElement(By.xpath("//*[@id=\"SubmitCreate\"]"));
		emailTxtBx = exampleDriver.findElement(By.xpath("//*[@id=\"email_create\"]"));
	}
	
	// Create account option method
	public void selectCreateAccountOption(String email) {
		emailTxtBx.sendKeys(email);
		createAccountBtn.click();
	}
	
	
	
	
	

}
