package Web_Automation;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import java.time.Duration;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

/**
 * @author Sihlesenkosi
 * This is a test class
 */

public class AutomationTest {
	WebDriver driver;
	HomePage home;
	WebDriverWait wait;
	
	/*
	 * Test : Searching for a product using a string word.Clicks on the search button to complete
	 * the search
	 */
  @Test
  public void searchProducts() {
	  home = new HomePage(driver);
	  home.searchForItem("casual");
  }
  
  /*
   *Test : Hover over the Women' menu,hover over the Dresses menu and navigates back to Home by clicking the 
   * site logo- link
   * 
   */
  @Test
  public void hoverThroughMenu() {
	  home = new HomePage(driver);
	  home.hoverWomenMenu(driver);
	  home.hoverDressesMenu(driver);
	  home.navigatetoHomepage();
  }
  
  /*
   * Test: Creates a user account for purchase.This tests provides email address and clicks on Create 
   * account button
   */
  @Test
  public void createShoppingAccount() {
	  home = new HomePage(driver);
	  home.clickSignInButton();
	  CheckoutPage checkout = new CheckoutPage(driver);
	  checkout.selectCreateAccountOption("sihlesenkosibthlz@gmail.com");
	  wait = new WebDriverWait(driver,Duration.ofSeconds(60));
	 
  }
  
  /*
   * Initializes the webdriver and gets the test site URL
   */
  @BeforeTest
  public void beforeTest() {
	  WebDriverManager.chromedriver().setup();
	  driver = new ChromeDriver();
	  driver.get("http://automationpractice.com/index.php");
  }

  /*
   * Ends the test, closes the driver.
   */
  @AfterTest
  public void afterTest() {
	  driver.close();
  }
 

}
