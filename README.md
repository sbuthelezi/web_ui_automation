### This is a README.md file for WEB UI Automation###

### This project was created on Eclipse IDE for Enterprise Java Developers Version 2020-12

### How to pull the repo to the Eclipse IDE

Pull the repository to Eclipse as per below
-Open Eclipse
-Navigate to GIT perspective - Window->Perspective->Other->Git
-Select Clone a repository icon
-On the URI paste the Repo URI link and Click next until the button Finish is clickable
-Navigate to Java perspective - Window->Perspective->Other->Java
-Import project from existing Git repository that you added above
-Once the import project has been completed, ensure that the below plugins are installed on your Eclipse 
 -TestNG plugin
 -Buildship Gradle Integration 3.0

Right click on the build.gradle file,select Gradle -> Refresh Gradle Project

### To Run the project

-Navigate to src/test/java and expand the Web_Automation package
-Open the AutomationTest file to run the test.
-Click on the Run command to run the tests


### UI Tests

Automation has 3 tests

1.searchProduct
-This tests the search functionality of the site under test

2.hoverThroughMenu
 This test hovers through the menus

3.createShoppingAccount
-This tests that user can add email and click the create account button


